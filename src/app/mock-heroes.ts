import { Hero } from './hero';

export const HEROES: Hero[] = [
    {id: 1, name: 'Yohana'},
    {id: 12, name: 'Muganga'},
    {id: 45, name: 'Raphie'},
    {id: 46, name: 'Who are you'}
];
